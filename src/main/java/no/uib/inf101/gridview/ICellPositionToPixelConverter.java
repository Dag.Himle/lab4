package no.uib.inf101.gridview;

import no.uib.inf101.colorgrid.CellPosition;

import java.awt.geom.Rectangle2D;

public interface ICellPositionToPixelConverter {

    /**
     * Returns the rectangle in which the cell at position pos should be drawn
     * Rectangle2D is a class in the java.awt.geom package
     * 
     * @param pos the position of the cell
     * @return the rectangle in which the cell at position pos should be drawn
     * @throws IndexOutOfBoundsException if the position is out of bounds
     */
    public Rectangle2D getBoundsForCell(CellPosition pos);
}
