package no.uib.inf101.gridview;

import no.uib.inf101.colorgrid.CellPosition;
import no.uib.inf101.colorgrid.GridDimension;

import java.awt.geom.Rectangle2D; 


public class CellPositionToPixelConverter implements ICellPositionToPixelConverter{

  Rectangle2D box;
  GridDimension gd;
  double margin;

  // box is the rectangle in which the grid should be drawn
  // gd is the grid dimension
  // margin is the margin between the grid and the edges of the box
  // (margin is the same for all sides)
  public CellPositionToPixelConverter(Rectangle2D box, GridDimension gd, double margin) {
    this.box = box;
    this.gd = gd;
    this.margin = margin;
  }

  @Override
  public Rectangle2D getBoundsForCell(CellPosition pos) {
    double cellWidth = (box.getWidth() - (margin * (gd.cols() + 1))) / gd.cols();
    double cellHeight = (box.getHeight() - (margin * (gd.rows() + 1))) / gd.rows();

    double x = box.getX() + (margin * (pos.col() + 1)) + (cellWidth * pos.col());
    double y = box.getY() + (margin * (pos.row() + 1)) + (cellHeight * pos.row());

    return new Rectangle2D.Double(x, y, cellWidth, cellHeight);

  }
}
