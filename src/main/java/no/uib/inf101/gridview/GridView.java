package no.uib.inf101.gridview;

import no.uib.inf101.colorgrid.IColorGrid;
import no.uib.inf101.colorgrid.CellColorCollection;
import no.uib.inf101.colorgrid.CellColor;

import javax.swing.JPanel;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Dimension;
import java.awt.geom.Rectangle2D;

public class GridView extends JPanel {

  IColorGrid grid;

  public GridView(IColorGrid grid) {
    this.setPreferredSize(new Dimension(400, 300));
    this.setBackground(Color.WHITE);

    this.grid = grid;

  }

  @Override
  public void paintComponent(Graphics g) {
    super.paintComponent(g);
    
    // Cast to Graphics2D
    Graphics2D g2d = (Graphics2D) g;
    // Draw grid
    drawGrid(g2d);
  }

  // Draw the grid background and all cells
  // The grid background is drawn in light gray
  // The cells are drawn in the color specified by the grid
  private void drawGrid(Graphics2D g2d) {
    double margin = 15;
    double x, y; 
    x = y = margin;

    double width = this.getWidth() - 2 * margin;
    double height = this.getHeight() - 2 * margin;
    g2d.setColor(Color.LIGHT_GRAY);
    g2d.fill(new Rectangle2D.Double(x, y, width, height));

    CellPositionToPixelConverter converter = new CellPositionToPixelConverter(new Rectangle2D.Double(x, y, width, height), grid, margin);
    drawCells(g2d, grid, converter);
  }

  // Help function to draw all cells in the grid
  // If no color is specified for a cell, draw it in dark gray
  // (this is the default color for cells)
  private static void drawCells(Graphics2D g2d, CellColorCollection grid, CellPositionToPixelConverter converter) {
    for (CellColor currcell : grid.getCells()) {
      Rectangle2D shape = converter.getBoundsForCell(currcell.pos());
      if (currcell.color() != null){
        g2d.setColor(currcell.color());
      }else{
        g2d.setColor(Color.DARK_GRAY);
      }
      g2d.fill(shape);
    }
  }
}
