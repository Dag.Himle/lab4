package no.uib.inf101.colorgrid;

import java.awt.Color;
import java.util.List;
import java.util.ArrayList;



//  A grid of colors
public class ColorGrid implements IColorGrid{

  private int width; // width of the grid
  private int height; // height of the grid
  public List<List<CellColor>> grid; // the grid

  // Create a new grid with the given dimensions
  public ColorGrid(int height, int width) {
    this.height = height;
    this.width = width;

    this.grid = new ArrayList<List<CellColor>>(height);
    for (int i = 0; i < height; i++) {
      this.grid.add(new ArrayList<CellColor>(width));
      for (int j = 0; j < width; j++) {
        this.grid.get(i).add(new CellColor(new CellPosition(i, j), null));
      }
    }
  }
  

  @Override
  public int rows() {
    return height;
  }

  @Override
  public int cols() {
    return width;
  }
  
  
  @Override
  public Color get(CellPosition pos) {
    return grid.get(pos.row()).get(pos.col()).color();
  }
  
      
  @Override
  public void set(CellPosition pos, Color color) {
    grid.get(pos.row()).set(pos.col(), new CellColor(pos, color));
  }

  @Override
  public List<CellColor> getCells() {
    List<CellColor> cells = new ArrayList<CellColor>();
    for (int i = 0; i < height; i++) {
      for (int j = 0; j < width; j++) {
        cells.add(grid.get(i).get(j));
      }
    }
    return cells; 
  }
}
